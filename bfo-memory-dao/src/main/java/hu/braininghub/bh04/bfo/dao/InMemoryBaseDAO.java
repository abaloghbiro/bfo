/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.dao;

import hu.braininghub.bh04.bfo.model.BusinessObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Attila
 */
public abstract class InMemoryBaseDAO<T extends BusinessObject> implements BaseDAO<T> {

    protected final Map<Class<?>, List<T>> STORAGE = new HashMap<>();

    @Override
    public void save(T object) {

        List<T> list = STORAGE.get(object.getClass());

        if (list == null) {
            list = new ArrayList<>();
            STORAGE.put(object.getClass(), list);
        }
        list.add(object);
    }

    @Override
    public void delete(Long id) {

        List<T> objectList = STORAGE.get(getClazz());
        for (T o : objectList) {
            if (o.getId() == id) {
                objectList.remove(o);
            }
        }
    }

    @Override
    public T getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<T> getAll() {
        return STORAGE.get(getClazz());
    }

    @Override
    public void update(T object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected abstract Class<?> getClazz();
}

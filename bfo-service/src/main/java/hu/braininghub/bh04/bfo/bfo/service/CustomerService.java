/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.bfo.service;

import hu.braininghub.bh04.bfo.service.dto.CustomerCreationRequest;

/**
 *
 * @author Attila
 */
public interface CustomerService {
    
    
    Long createCustomerAndOpenNewAccount(CustomerCreationRequest request);
}

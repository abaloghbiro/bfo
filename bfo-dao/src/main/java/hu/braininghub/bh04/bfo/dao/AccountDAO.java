/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.dao;

import hu.braininghub.bh04.bfo.model.Account;

/**
 *
 * @author Attila
 */
public interface AccountDAO extends BaseDAO<Account>{
    
}

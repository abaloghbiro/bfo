/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.dao;

import hu.braininghub.bh04.bfo.model.Transaction;
import hu.braininghub.bh04.bfo.model.TransactionStatus;
import java.util.List;

/**
 *
 * @author Attila
 */
public interface TransactionDAO extends BaseDAO<Transaction> {
 
    List<Transaction> getTransactionsBasedOnStatusAndCustomerName(TransactionStatus status,String targetAccountCustomerName);
}

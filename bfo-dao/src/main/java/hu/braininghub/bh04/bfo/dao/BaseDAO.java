/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.dao;

import hu.braininghub.bh04.bfo.model.BusinessObject;
import java.util.List;

/**
 *
 * @author Attila
 * @param <T>
 */
public interface BaseDAO<T extends BusinessObject> {
    
    void save(T object);
    
    void delete(Long id);
    
    T getById(Long id);
    
    List<T> getAll();
    
    void update(T object);
}

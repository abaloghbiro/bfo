/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.model;

import java.util.Objects;


/**
 *
 * @author Attila
 */
public class Organization extends BusinessObject{

    private String organizationName;
    private Address address;
    private final String ibanNumber;
    private final String swiftCode;
    private final Ledger ledger;
    
    
    public Organization(Long id, String ibanNumber, String swiftCode, Ledger ledger){
        super(id);
        this.ibanNumber = ibanNumber;
        this.swiftCode = swiftCode;
        this.ledger = ledger;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getIbanNumber() {
        return ibanNumber;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public Ledger getLedger() {
        return ledger;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.organizationName);
        hash = 11 * hash + Objects.hashCode(this.address);
        hash = 11 * hash + Objects.hashCode(this.ibanNumber);
        hash = 11 * hash + Objects.hashCode(this.swiftCode);
        hash = 11 * hash + Objects.hashCode(this.ledger);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Organization other = (Organization) obj;
        if (!Objects.equals(this.organizationName, other.organizationName)) {
            return false;
        }
        if (!Objects.equals(this.ibanNumber, other.ibanNumber)) {
            return false;
        }
        if (!Objects.equals(this.swiftCode, other.swiftCode)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.ledger, other.ledger)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Organization{" + "organizationName=" + organizationName + ", address=" + address + ", ibanNumber=" + ibanNumber + ", swiftCode=" + swiftCode + ", ledger=" + ledger + '}';
    }

   
    
    
}

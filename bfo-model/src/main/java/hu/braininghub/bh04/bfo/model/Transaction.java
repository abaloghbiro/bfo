/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Attila
 */
public class Transaction extends BusinessObject {

    private final String sourceAccountNumber;
    private final String targetAccountNumber;
    private final String targetAccountCustomerName;
    private final Operation operation;
    private final Double amount;
    private final String currency;
    private final Date time;
    private TransactionStatus status;

    public Transaction(Long id, String sourceAccountNumber, String targetAccountNumber, String targetAccountCustomerName, Operation operation, Double amount, String currency, Date time) {
        super(id);
        this.sourceAccountNumber = sourceAccountNumber;
        this.targetAccountNumber = targetAccountNumber;
        this.targetAccountCustomerName = targetAccountCustomerName;
        this.operation = operation;
        this.amount = amount;
        this.currency = currency;
        this.time = time;
        this.status = TransactionStatus.IN_PROGRESS;

    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }
    
    public String getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public String getTargetAccountNumber() {
        return targetAccountNumber;
    }

    public String getTargetAccountCustomerName() {
        return targetAccountCustomerName;
    }

    public Operation getOperation() {
        return operation;
    }

    public Double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public Date getTime() {
        return time;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.sourceAccountNumber);
        hash = 97 * hash + Objects.hashCode(this.targetAccountNumber);
        hash = 97 * hash + Objects.hashCode(this.targetAccountCustomerName);
        hash = 97 * hash + Objects.hashCode(this.operation);
        hash = 97 * hash + Objects.hashCode(this.amount);
        hash = 97 * hash + Objects.hashCode(this.currency);
        hash = 97 * hash + Objects.hashCode(this.time);
        hash = 97 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transaction other = (Transaction) obj;
        if (!Objects.equals(this.sourceAccountNumber, other.sourceAccountNumber)) {
            return false;
        }
        if (!Objects.equals(this.targetAccountNumber, other.targetAccountNumber)) {
            return false;
        }
        if (!Objects.equals(this.targetAccountCustomerName, other.targetAccountCustomerName)) {
            return false;
        }
        if (!Objects.equals(this.currency, other.currency)) {
            return false;
        }
        if (this.operation != other.operation) {
            return false;
        }
        if (!Objects.equals(this.amount, other.amount)) {
            return false;
        }
        if (!Objects.equals(this.time, other.time)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Transaction{" + "sourceAccountNumber=" + sourceAccountNumber + ", targetAccountNumber=" + targetAccountNumber + ", targetAccountCustomerName=" + targetAccountCustomerName + ", operation=" + operation + ", amount=" + amount + ", currency=" + currency + ", time=" + time + ", status=" + status + '}';
    }

}

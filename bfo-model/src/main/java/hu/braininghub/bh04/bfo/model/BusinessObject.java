/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.model;

/**
 *
 * @author Attila
 */
public abstract class BusinessObject {
    
    private final Long id;
    private Boolean active;
    
    protected BusinessObject(Long id){
        this.id = id;
        this.active = Boolean.TRUE;
    }

    public Long getId() {
        return id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
    
    
    
}

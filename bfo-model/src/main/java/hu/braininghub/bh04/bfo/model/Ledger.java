/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.model;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author Attila
 */
public class Ledger extends BusinessObject {
    
    private List<Transaction> transactions;
    
    public Ledger(Long id){
        super(id);
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.transactions);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ledger other = (Ledger) obj;
        if (!Objects.equals(this.transactions, other.transactions)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Ledger{" + "transactions=" + transactions + '}';
    }
    
    
    
    
}

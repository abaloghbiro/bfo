/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.model;

import java.util.Objects;

/**
 *
 * @author Attila
 */
public class Account extends BusinessObject{
    
    private final Long firstEightNumber;
    private final Long secondEightNumber;
    private final Long thirdEightNumber;
    private Double currentBalance;
    private final AccountType accountType;
    private final Organization organization;
    private final String currency;
    
    public Account(Long id,Organization org,Long firstEightNumber,Long secondEightNumber,Long thirdEightNumber, AccountType type,String currency){
        super(id);
        this.organization = org;
        this.firstEightNumber = firstEightNumber;
        this.secondEightNumber = secondEightNumber;
        this.thirdEightNumber =  thirdEightNumber;
        this.accountType = type;
        this.currency = currency;
        this.currentBalance = 0.0d;
    }

    public Long getFirstEightNumber() {
        return firstEightNumber;
    }

    public Long getSecondEightNumber() {
        return secondEightNumber;
    }

    public Long getThirdEightNumber() {
        return thirdEightNumber;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public Organization getOrganization() {
        return organization;
    }

    public String getCurrency() {
        return currency;
    }

    public Double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.firstEightNumber);
        hash = 89 * hash + Objects.hashCode(this.secondEightNumber);
        hash = 89 * hash + Objects.hashCode(this.thirdEightNumber);
        hash = 89 * hash + Objects.hashCode(this.currentBalance);
        hash = 89 * hash + Objects.hashCode(this.accountType);
        hash = 89 * hash + Objects.hashCode(this.organization);
        hash = 89 * hash + Objects.hashCode(this.currency);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Account other = (Account) obj;
        if (!Objects.equals(this.currency, other.currency)) {
            return false;
        }
        if (!Objects.equals(this.firstEightNumber, other.firstEightNumber)) {
            return false;
        }
        if (!Objects.equals(this.secondEightNumber, other.secondEightNumber)) {
            return false;
        }
        if (!Objects.equals(this.thirdEightNumber, other.thirdEightNumber)) {
            return false;
        }
        if (!Objects.equals(this.currentBalance, other.currentBalance)) {
            return false;
        }
        if (this.accountType != other.accountType) {
            return false;
        }
        if (!Objects.equals(this.organization, other.organization)) {
            return false;
        }
        return true;
    }

    
   
    @Override
    public String toString() {
        return "Account{" + "firstEightNumber=" + firstEightNumber + ", secondEightNumber=" + secondEightNumber + ", thirdEightNumber=" + thirdEightNumber + ", currentBalance=" + currentBalance + ", accountType=" + accountType + ", organization=" + organization + ", currency=" + currency + '}';
    }
    
    
}

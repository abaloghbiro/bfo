/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.model;

/**
 *
 * @author Attila
 */
public enum TransactionStatus {
 
    IN_PROGRESS, SUCCESS, FAILED
}

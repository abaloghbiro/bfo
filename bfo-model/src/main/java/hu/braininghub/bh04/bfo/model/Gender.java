/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.model;

/**
 *
 * @author Attila
 */
public enum Gender {

    MALE(1), FEMALE(2);

    private final int code;

    private Gender(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.bfo.service;

import hu.braininghub.bh04.bfo.bfo.service.CustomerService;
import hu.braininghub.bh04.bfo.dao.AccountDAO;
import hu.braininghub.bh04.bfo.dao.CustomerDAO;
import hu.braininghub.bh04.bfo.service.dto.CustomerCreationRequest;

/**
 *
 * @author Attila
 */
public class DefaultCustomerService implements CustomerService {

    private final CustomerDAO dao;

    private final AccountDAO accDao;

    public DefaultCustomerService(CustomerDAO dao, AccountDAO accDao) {
        this.dao = dao;
        this.accDao = accDao;
    }

    @Override
    public Long createCustomerAndOpenNewAccount(CustomerCreationRequest request) {

        return 1L;
    }

}
